function addFeature(featureMessage) {
    var ul = document.getElementById("features");
    var li = document.createElement("li");
    li.innerHTML = featureMessage;
    ul.appendChild(li);
}

addFeature("The HTML5 video element is supported.");