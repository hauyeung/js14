window.onload = init;

function init() {
		Modernizr.load([            
            {
                test: Modernizr.geolocation,
                yep : "geolocation.js",
                nope: "nogeolocation.js",				
            },
            {
                test: Modernizr.localstorage,
                yep : "localStorage.js",
                nope: "noLocalStorage.js",
				
            },
			{
                test: Modernizr.canvas,
                yep : "canvas.js",
                nope: "nocanvas.js",
				
            },
			{
                test: Modernizr.video,
                yep : "video.js",
                nope: "novideo.js",
				
            }
		]);
}

function addFeature(featureMessage) {
    var ul = document.getElementById("features");
    var li = document.createElement("li");
    li.innerHTML = featureMessage;
    ul.appendChild(li);
}